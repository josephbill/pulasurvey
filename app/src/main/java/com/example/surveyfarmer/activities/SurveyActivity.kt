package com.example.surveyfarmer.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import cn.pedant.SweetAlert.SweetAlertDialog
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.surveyfarmer.R
import com.example.surveyfarmer.adapter.QuestionsAdapter
import com.example.surveyfarmer.adapter.SqliteAdapter
import com.example.surveyfarmer.apis.VolleySingleton
import com.example.surveyfarmer.databases.DatabaseHelper
import com.example.surveyfarmer.databases.DatabaseHelperPost
import com.example.surveyfarmer.models.QuestionModel
import kotlinx.android.synthetic.main.activity_survey.*
import org.json.JSONException
import org.json.JSONObject


class SurveyActivity : AppCompatActivity() {
    lateinit var prefs: SharedPreferences
    var runAlready: Boolean = false
    lateinit var context: Context
    var questions: List<QuestionModel>? = null
    private var arrayList: ArrayList<QuestionModel>? = null
    private var q_adapter: QuestionsAdapter? = null
    var questionId:String = ""; var question_type: String = ""; var nameQuestion: String = ""; var genderQuestion: String = ""; var optMale: String = "";
    var optFemale: String = ""; var optOther: String = ""; var sizeQuestion: String = ""; var question_text:String = ""; var next: String = "";

    var result : String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey)

        pb!!.setVisibility(View.GONE)

        //ref the recycler view widget and set the adapter to it
//        coronaRecycler.adapter = CoronaAdapter(this,listRecycler)
        //give our recycler items a view group
        //recyclerview.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        // Calling the override functions from
        // the Linear Layout Manager inner class
        val myLinearLayoutManager = object : LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        ) {
            override fun canScrollHorizontally(): Boolean {
                return false
            }
        }
        recyclerview.layoutManager = myLinearLayoutManager
        //set a fixed item size
        recyclerview.setHasFixedSize(true)


        arrayList = ArrayList()



        if (isNetworkConnected()){
            fetchData()
            Handler().postDelayed({
                val databaseHelperPost = DatabaseHelperPost(this)
                result = databaseHelperPost.getAnswers()
                if (!result.isEmpty()){
                    postToDummy(result)
                } else {
                    Toast.makeText(this,"Nothing to submit",Toast.LENGTH_LONG).show()
                }

            }, 3000) // 3 seconds delay

        } else {
            fetchFromSQLite()
            Toast.makeText(applicationContext, "no internet", Toast.LENGTH_LONG).show()
        }




    }

    private fun postToDummy(result: String) {
        val url = "https://postman-echo.com/post"
        // Post parameters
        // Form fields and values
        val params = HashMap<String, String>()
        params["foo1"] = result
        val jsonObject = JSONObject(params as Map<*, *>)

        val request = JsonObjectRequest(Request.Method.POST,url,jsonObject, Response.Listener { response ->
            //capture success
            // Process the json
            try {
                Log.d("message", "Response: $response")
                Toast.makeText(this,"Submit successful",Toast.LENGTH_LONG).show()
            } catch (e: Exception) {
                Log.d("message", "Exception: $e")
            }
        }, Response.ErrorListener {
            //capture failure
            Log.d("message", "Exception: $it")

            Toast.makeText(this,"Error , check internet",Toast.LENGTH_LONG).show()

        })

        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
            // 0 means no retry
            0, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )


        //adding request to volley
        VolleySingleton.getInstance(this).addToRequestQueue(request)
    }


    private fun fetchFromSQLite() {
        val databaseHelper = DatabaseHelper(this)
        arrayList = ArrayList(databaseHelper.readData())
        val myLinearLayoutManager = object : LinearLayoutManager(
            this
        ) {
            override fun canScrollVertically(): Boolean {
                return true
            }
        }
        recyclerview.layoutManager = myLinearLayoutManager
        recyclerview.setItemAnimator(DefaultItemAnimator())
        val adapter = SqliteAdapter(applicationContext, arrayList!!)
        recyclerview.setAdapter(adapter)

    }



    fun scrollRV(){
        if ((recyclerview.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() < q_adapter!!.getItemCount() - 1)
        {
            (recyclerview.layoutManager as LinearLayoutManager).scrollToPosition((recyclerview.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() + 1)
        }

    }

    override fun onBackPressed() {
        if ((recyclerview.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() < q_adapter!!.getItemCount() + 1)
        {
            (recyclerview.layoutManager as LinearLayoutManager).scrollToPosition((recyclerview.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() - 1)
        } else {
            finishAffinity()
        }
    }


    private fun fetchData(){
        val loadingDialog = SweetAlertDialog(this@SurveyActivity, SweetAlertDialog.PROGRESS_TYPE)
        loadingDialog.setTitleText("Loading ...") //Processing your request
        loadingDialog.setCancelable(true)
        loadingDialog.setCanceledOnTouchOutside(false)
        loadingDialog.show()
        val url = "https://run.mocky.io/v3/d628facc-ec18-431d-a8fc-9c096e00709a"
        val request = JsonObjectRequest(Request.Method.GET, url, null,
            Response.Listener { response ->
                loadingDialog.dismiss()
                try {
                    val jsonArray = response.getJSONArray("questions")
                    Log.d("array", "response is $jsonArray")
                    for (i in 0 until jsonArray.length()) {
                        val hit = jsonArray.getJSONObject(i)
                        questionId = hit.getString("id")
                        question_type = hit.getString("question_type")
                        question_text = hit.getString("question_text")
                        val questionObject = response.getJSONObject("strings")
                        val questions = questionObject.getJSONObject("en")
                        nameQuestion = questions.getString("q_farmer_name")
                        genderQuestion = questions.getString("q_farmer_gender")
                        sizeQuestion = questions.getString("q_size_of_farm")
                        optMale = questions.getString("opt_male")
                        optFemale = questions.getString("opt_female")
                        optOther = questions.getString("opt_other")
                        Log.d("text", "text from json " + question_text)




                        arrayList!!.add(
                            QuestionModel(
                                questionId,
                                question_type,
                                question_text,
                                nameQuestion,
                                genderQuestion,
                                sizeQuestion,
                                optMale,
                                optFemale,
                                optOther
                            )
                        )

                        //saving to sqlite db
                        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
                        if (!prefs.getBoolean("firstTime", false)) {
                            // <---- run your one time code here
                            if (runAlready === false) {
                                val databaseHelper = DatabaseHelper(this@SurveyActivity)
                                val status = databaseHelper.addUsers(
                                    QuestionModel(
                                        questionId,
                                        question_type,
                                        question_text,
                                        nameQuestion,
                                        genderQuestion,
                                        sizeQuestion,
                                        optMale,
                                        optFemale,
                                        optOther
                                    )
                                )
                                if (status > -1) {
                                    Toast.makeText(
                                        applicationContext, "Questions saved",
                                        Toast.LENGTH_LONG
                                    ).show()
                                } else {
                                    Toast.makeText(
                                        applicationContext, "Something went wrong ,try again",
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                                runAlready = true
                            }

                            // mark first time has ran.
                            val editor = prefs.edit()
                            editor.putBoolean("firstTime", true)
                            editor.commit()
                        }



                    }


                    //adding model data to adapter
                    q_adapter = arrayList?.let { QuestionsAdapter(this@SurveyActivity, it) }
                    recyclerview!!.adapter = q_adapter
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }, Response.ErrorListener { error ->
                error.printStackTrace()
            })
        VolleySingleton.getInstance(this@SurveyActivity).addToRequestQueue(request)
    }



    private fun isNetworkConnected(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager.activeNetwork
        } else {
            TODO("VERSION.SDK_INT < M")
        }
        val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)
        return networkCapabilities != null && networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
    }
}