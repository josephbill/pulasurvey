package com.example.surveyfarmer.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.surveyfarmer.R
import com.example.surveyfarmer.activities.SurveyActivity
import com.example.surveyfarmer.apis.VolleySingleton
import com.example.surveyfarmer.databases.DatabaseHelperPost
import com.example.surveyfarmer.models.AnswersModel
import com.example.surveyfarmer.models.QuestionModel
import kotlinx.android.synthetic.main.question_item.view.*
import kotlinx.android.synthetic.main.question_item.view.button
import kotlinx.android.synthetic.main.question_item.view.questionTextView
import kotlinx.android.synthetic.main.question_item.view.radioButton
import kotlinx.android.synthetic.main.question_item.view.radioButton2
import kotlinx.android.synthetic.main.question_item.view.radioButton3
import kotlinx.android.synthetic.main.question_item.view.textInputLayout2
import kotlinx.android.synthetic.main.questionitem2.view.*
import org.json.JSONObject

class SqliteAdapter (private val context: Context, private val questionlist: ArrayList<QuestionModel>) :
    RecyclerView.Adapter<SqliteAdapter.RecyclerViewHolder>() {
    //variables to store user input
    var name: String = ""
    var gender: String = ""
    var size: String = ""
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SqliteAdapter.RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(
            R.layout.questionitem2, parent, false
        )
        return RecyclerViewHolder(inflater)
    }

    override fun getItemCount() = questionlist.size


    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        //create a var that will ref the item position
        val items = questionlist[position]


        val question_text = items.questionText

        holder.textQuestion.text = items.farmerName
        holder.textQuestion2.text = items.farmerGender
        holder.textQuestion3.text = items.farmSize
        holder.btnSubmit.text = "Submit Survey"

        holder.btnSubmit.setOnClickListener {
            name = holder.textResponse2.text.toString()
            if (holder.option1.isChecked){
                gender = items.optMale
            } else if(holder.option2.isChecked) {
                gender = items.optFemale
            } else if (holder.option3.isChecked){
                gender = items.optOther

            }
            size = holder.textResponse.text.toString()

            Toast.makeText(context, "details are " + name + size + gender.toString(), Toast.LENGTH_LONG).show()

            submitToSql(name,gender,size)


        }

        Log.d("id", "id is " + items.questionID + items.farmerName + items.farmerGender)

    }

    private fun submitToSql(name: String, gender: String, size: String) {
        val databaseHelper = DatabaseHelperPost(context)
        val status = databaseHelper.addUsers(AnswersModel(name,gender,size))
        if (status > -1) {
            Toast.makeText(
                context, "Responses saved",
                Toast.LENGTH_LONG
            ).show()
        } else {
            Toast.makeText(
                context, "Something went wrong ,try again",
                Toast.LENGTH_LONG
            ).show()
        }
    }




    //    declare view holder
    class RecyclerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //declaring variables to ref the views in my recycled item
        val textQuestion = itemView.questionTextView;
        val textQuestion2 = itemView.questionTextView2;
        val textQuestion3 = itemView.questionTextView3;
        val textLayout = itemView.textInputLayout2
        val textResponse = itemView.questionResponse3
        val textResponse2 = itemView.questionResponse2
        val option1 = itemView.radioButton
        val option2 = itemView.radioButton2
        val option3 = itemView.radioButton3
        val btnSubmit = itemView.button

    }

}