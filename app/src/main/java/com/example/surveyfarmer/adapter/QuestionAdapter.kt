package com.example.surveyfarmer.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import cn.pedant.SweetAlert.SweetAlertDialog
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.surveyfarmer.R
import com.example.surveyfarmer.activities.SurveyActivity
import com.example.surveyfarmer.apis.VolleySingleton
import com.example.surveyfarmer.databases.DatabaseHelper
import com.example.surveyfarmer.databases.DatabaseHelperPost
import com.example.surveyfarmer.models.AnswersModel
import com.example.surveyfarmer.models.QuestionModel
import kotlinx.android.synthetic.main.question_item.view.*
import org.json.JSONObject


class QuestionsAdapter(
    private val context: Context,
    private val questionlist: ArrayList<QuestionModel>
) :
    RecyclerView.Adapter<QuestionsAdapter.RecyclerViewHolder>()
{
    //variables to store user input
    var name: String = ""
     var gender: String = ""
    var size: String = ""
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): QuestionsAdapter.RecyclerViewHolder {
        val inflater = LayoutInflater.from(parent.context).inflate(
            R.layout.question_item, parent, false
        )
        return RecyclerViewHolder(inflater)
    }

    override fun getItemCount() = questionlist.size

    override fun onBindViewHolder(holder: QuestionsAdapter.RecyclerViewHolder, position: Int) {
        //create a var that will ref the item position
        val items = questionlist[position]


        val question_text = items.questionText

        if (question_text.equals("q_farmer_name")){
            holder.textQuestion.text = items.farmerName
        } else if (question_text.equals("q_farmer_gender")){
            holder.textQuestion.text = items.farmerGender
        } else if (question_text.equals("q_size_of_farm")){
            holder.textQuestion.text = items.farmSize
        }

        //input type
        if (question_text.equals("q_farmer_name")){
            holder.textLayout.visibility = View.VISIBLE
        } else if (question_text.equals("q_farmer_gender")){
            holder.option1.visibility = View.VISIBLE
            holder.option1.text = items.optMale
            holder.option2.text = items.optFemale
            holder.option3.text = items.optOther
            holder.option2.visibility = View.VISIBLE
            holder.option3.visibility = View.VISIBLE
        } else if (question_text.equals("q_size_of_farm")){
            holder.textLayout.visibility = View.VISIBLE
            holder.btnSubmit.text = "Submit Survey"
        }

        if (question_text.equals("q_farmer_name")){
            holder.btnSubmit.setOnClickListener {
                (context as SurveyActivity).scrollRV()
                name = holder.textResponse.text.toString()
            }
        } else if (question_text.equals("q_farmer_gender")){
            holder.btnSubmit.setOnClickListener {
                (context as SurveyActivity).scrollRV()
                if (holder.option1.isChecked){
                    gender = items.optMale
                } else if(holder.option2.isChecked) {
                    gender = items.optFemale
                } else if (holder.option3.isChecked){
                    gender = items.optOther

                }
                Toast.makeText(
                    context, gender.toString(),
                    Toast.LENGTH_LONG
                ).show()
            }
        } else if(question_text.equals("q_size_of_farm")){
            holder.btnSubmit.setOnClickListener {
                size = holder.textResponse.text.toString()
                Toast.makeText(context, "details are " + name + size + gender.toString(), Toast.LENGTH_LONG).show()
                submitToSql(name,gender,size)

            }
        }




        Log.d("id", "id is " + items.questionID)


    }

    private fun submitToSql(name: String, gender: String, size: String) {
          val databaseHelper = DatabaseHelperPost(context)
          val status = databaseHelper.addUsers(AnswersModel(name,gender,size))
          if (status > -1) {
            Toast.makeText(
                context, "Responses saved",
                Toast.LENGTH_LONG
            ).show()
           } else {
            Toast.makeText(
                context, "Something went wrong ,try again",
                Toast.LENGTH_LONG
            ).show()
           }
    }



    //    declare view holder
    class RecyclerViewHolder(itemView: View)  : RecyclerView.ViewHolder(itemView){
        //declaring variables to ref the views in my recycled item
        val textQuestion = itemView.questionTextView;
        val textLayout = itemView.textInputLayout2
        val textResponse = itemView.questionResponse
        val option1 = itemView.radioButton
        val option2 = itemView.radioButton2
        val option3 = itemView.radioButton3
        val btnSubmit = itemView.button

    }


}