package com.example.surveyfarmer.databases

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import androidx.room.RoomMasterTable.TABLE_NAME
import com.example.surveyfarmer.models.QuestionModel
import java.util.*


class DatabaseHelper(context: Context) : SQLiteOpenHelper(
    context, DATABASE_NAME, null,
    DATABASE_VERSION
) {

    //define our constant variables
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "QuestionsDatabase"
        private val TABLE_QUESTIONS = "QuestionsTable"
        private val KEY_ID = "recordId"
        private val KEY_Q_ID = "questionID"
        private  val KEY_QUESTION_TYPE = "questionQType"
        private  val KEY_QUESTION_TEXT = "questionQText"
        private  val KEY_QUESTION_NAME = "questionQName"
        private  val KEY_QUESTION_GENDER = "questionQGender"
        private  val KEY_QUESTION_SIZE = "questionQSize"
        private  val KEY_QUESTION_MALE = "questionQMale"
        private  val KEY_QUESTION_FEMALE = "questionQFemale"
        private  val KEY_QUESTION_OTHER = "questionQOther"
        
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //define our query
        val CREATE_CONTACTS_TABLE =
            ("CREATE TABLE IF NOT EXISTS " + TABLE_QUESTIONS + "(" + KEY_ID + " INTEGER AUTO INCREMENT PRIMARY KEY," + KEY_Q_ID + " TEXT," + KEY_QUESTION_TYPE + " TEXT," + KEY_QUESTION_TEXT + " TEXT," + KEY_QUESTION_NAME + " TEXT," +
                    KEY_QUESTION_GENDER + " TEXT," + KEY_QUESTION_SIZE + " TEXT," + KEY_QUESTION_MALE + " TEXT," + KEY_QUESTION_FEMALE + " TEXT," + KEY_QUESTION_OTHER + " TEXT" + ")")
        //execute the query
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS" + TABLE_QUESTIONS)
        onCreate(db)
    }

    //save data
    fun addUsers(sqliteModel: QuestionModel): Long {
        //telling the db what to do
        val db = this.writableDatabase
        //define and place content
        val contentvalues = ContentValues()
        //put data to the respective fields
        contentvalues.put(KEY_Q_ID, sqliteModel.questionID)
        contentvalues.put(KEY_QUESTION_TYPE, sqliteModel.questionType)
        contentvalues.put(KEY_QUESTION_TEXT, sqliteModel.questionText)
        contentvalues.put(KEY_QUESTION_NAME, sqliteModel.farmerName)
        contentvalues.put(KEY_QUESTION_GENDER, sqliteModel.farmerGender)
        contentvalues.put(KEY_QUESTION_SIZE, sqliteModel.farmSize)
        contentvalues.put(KEY_QUESTION_MALE, sqliteModel.optMale)
        contentvalues.put(KEY_QUESTION_FEMALE, sqliteModel.optFemale)
        contentvalues.put(KEY_QUESTION_OTHER, sqliteModel.optOther)
       Log.d(
           "savedData",
           "saved data is " + sqliteModel.questionText + sqliteModel.farmSize + sqliteModel.farmerGender + sqliteModel.farmerName
       )
        //query to insert to db
        val success = db.insert(TABLE_QUESTIONS, null, contentvalues)
        //close the db connection
        db.close()
        //return output of method
        return success
    }

    //function to view details
    fun readData() : ArrayList<QuestionModel>{

        val arrayList: ArrayList<QuestionModel> = ArrayList<QuestionModel>()
        // select all query
        val select_query = "SELECT * FROM " + TABLE_QUESTIONS

        val db = this.writableDatabase
        val cursor = db.rawQuery(select_query, null)

        // looping through all rows and adding to list

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                val noteModel = QuestionModel(questionID = cursor.getString(1), questionType = cursor.getString(2), questionText = cursor.getString(3), farmerName = cursor.getString(4), farmerGender = cursor.getString(5),
               farmSize = cursor.getString(6), optMale = cursor.getString(7), optFemale = cursor.getString(8), optOther = cursor.getString(9))
                arrayList.add(noteModel)
            } while (cursor.moveToNext())
        }
        return arrayList
    }
}