package com.example.surveyfarmer.databases

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.surveyfarmer.models.AnswersModel
import com.example.surveyfarmer.models.QuestionModel

class DatabaseHelperPost(context: Context) : SQLiteOpenHelper(
    context, DATABASE_NAME, null,
    DATABASE_VERSION
) {

    //define our constant variables
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "AnswersDatabase"
        private val TABLE_ANSWERS = "AnswersTable"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_GENDER = "gender"
        private val KEY_SIZE = "size"


    }

    override fun onCreate(db: SQLiteDatabase?) {
        //define our query
        val CREATE_CONTACTS_TABLE =
            ("CREATE TABLE IF NOT EXISTS " + TABLE_ANSWERS + "(" + KEY_ID + " INTEGER AUTO INCREMENT PRIMARY KEY,"
                    + KEY_NAME + " TEXT," + KEY_GENDER + " TEXT," + KEY_SIZE + " TEXT" + ")")
        //execute the query
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS" + TABLE_ANSWERS)
        onCreate(db)
    }

    //save data
    fun addUsers(sqliteModel: AnswersModel): Long {
        //telling the db what to do
        val db = this.writableDatabase
        //define and place content
        val contentvalues = ContentValues()
        //put data to the respective fields
        contentvalues.put(KEY_NAME, sqliteModel.name)
        contentvalues.put(KEY_GENDER, sqliteModel.gender)
        contentvalues.put(KEY_SIZE, sqliteModel.size)

        Log.d(
            "savedData",
            "saved data is " + sqliteModel.name + sqliteModel.gender + sqliteModel.size
        )
        //query to insert to db
        val success = db.insert(TABLE_ANSWERS, null, contentvalues)
        //close the db connection
        db.close()
        //return output of method
        return success
    }

    //get all Answers
    fun getAnswers(): String {
        var allanswers: String = "";
        val db = readableDatabase
        val selectALLQuery = "SELECT * FROM $TABLE_ANSWERS"
        val cursor = db.rawQuery(selectALLQuery, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    var name = cursor.getString(cursor.getColumnIndex("name"))
                    var gender = cursor.getString(cursor.getColumnIndex("gender"))
                    var size = cursor.getString(cursor.getColumnIndex("size"))

                    allanswers = "$name\n$gender $size"
                } while (cursor.moveToNext())
            }
        }
        cursor.close()
        db.close()
        return allanswers
    }
}