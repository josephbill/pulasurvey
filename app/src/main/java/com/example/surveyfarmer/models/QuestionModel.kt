package com.example.surveyfarmer.models

class QuestionModel(val questionID: String, val questionType: String,
                     val questionText: String, val farmerName: String,
                     val farmerGender: String, val farmSize: String, val optMale: String,
                     val optFemale: String, val optOther: String) {
}